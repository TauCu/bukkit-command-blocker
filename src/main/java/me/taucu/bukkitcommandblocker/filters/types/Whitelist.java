package me.taucu.bukkitcommandblocker.filters.types;

import org.bukkit.command.CommandSender;

import me.taucu.bukkitcommandblocker.filters.AbstractFilter;
import me.taucu.bukkitcommandblocker.filters.FilterAction;
import me.taucu.bukkitcommandblocker.filters.Filters;

public class Whitelist extends AbstractFilter {
    
    public Whitelist(Filters parent, String name) {
        super(parent, name);
    }
    
    @Override
    public FilterAction apply(CommandSender sender, String command) {
        if (this.checkPermission(sender) || this.getMatcher(command).find()) {
            return allowAction;
        } else {
            return denyAction;
        }
    }
    
}
