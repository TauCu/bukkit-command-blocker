package me.taucu.bukkitcommandblocker.filters;

public enum FilterAction {
    
    ALLOW, DENY, SOFT_ALLOW, SOFT_DENY, NONE;

}
