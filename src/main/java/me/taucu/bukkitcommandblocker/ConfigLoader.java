package me.taucu.bukkitcommandblocker;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import com.google.common.io.ByteStreams;

import me.taucu.bukkitcommandblocker.filters.Filters;
import me.taucu.bukkitcommandblocker.utils.ConfigurationException;
import net.md_5.bungee.api.ChatColor;

public class ConfigLoader {
    
    private final File baseFile;
    private final File configFile;
    
    private final CommandBlockerPlugin plugin;
    private final Logger log;
    
    public ConfigLoader(CommandBlockerPlugin plugin, File baseFile) {
        this.plugin = plugin;
        this.baseFile = baseFile;
        this.configFile = new File(baseFile, "config.yml");
        this.log = plugin.getLogger();
    }
    
    /**
     * Used to reload the plugins configuration
     */
    public void reloadConfig() {
        baseFile.mkdirs();
        Configuration internalConfig = loadInternalConfig();
        Configuration config = loadConfig(internalConfig);
        ConfigurationSection filtersSection = config.getConfigurationSection("filters");
        
        if (filtersSection == null) {
            throw new ConfigurationException("\"filters\" is an invalid configuration section");
        }
        
        Filters filters = plugin.getFilters();
        
        filters.setDefaultPatternPrefix(config.getString("default pattern prefix"));
        filters.setDefaultPatternSuffix(config.getString("default pattern suffix"));
        filters.setDefaultType(config.getString("default type"));
        filters.setDefaultDenyMsg(ChatColor.translateAlternateColorCodes('&', config.getString("default deny message")));
        
        ConfigurationSection defaultActionsSection = config.getConfigurationSection("default actions");
        if (defaultActionsSection == null) {
            throw new ConfigurationException("\"default actions\" is an invalid configuration section");
        } else {
            filters.loadDefaultActions(defaultActionsSection);
        }
        
        filters.setFilterPerm(config.getString("root filter permission"));
        filters.setDefaultPermissionCacheMillis(config.getInt("permission cache millis"));
        filters.load(plugin.getLogger(), filtersSection);
    }
    
    public Configuration loadInternalConfig() {
        try (InputStream in = plugin.getResource("config.yml")) {
            return YamlConfiguration.loadConfiguration(new InputStreamReader(in));
        } catch (IOException e) {
            throw new ConfigurationException("could not load internal config", e);
        }
    }
    
    public Configuration loadConfig(Configuration defaults) throws ConfigurationException {
        if (!configFile.isFile()) {
            try {
                saveDefaultConfig();
            } catch (IOException e) {
                throw new ConfigurationException("unable to save default config", e);
            }
        }
        Configuration config;
        try {
            try {
                config = YamlConfiguration.loadConfiguration(configFile);
                config.setDefaults(defaults);
            } catch (Exception e) {
                log.log(Level.SEVERE, "Exception while loading config, attempting to regenerate it.", e);
                regenerateConfig();
                config = YamlConfiguration.loadConfiguration(configFile);
                config.setDefaults(defaults);
            }
            
            double defaultVer = defaults.getDouble("config version");
            double fileVer = config.getDouble("config version");
            if (defaultVer != fileVer) {
                if ((int) defaultVer != (int) fileVer) {
                    log.warning("outdated configuration major version, regenerating.");
                    regenerateConfig();
                    config = YamlConfiguration.loadConfiguration(configFile);
                    config.setDefaults(defaults);
                } else {
                    log.warning("outdated configuration minor version. it is recommended to regenerate the config.");
                }
            }
            return config;
        } catch (Exception e) {
            throw new ConfigurationException("unable to load config", e);
        }
    }
    
    public void regenerateConfig() {
        try {
            for (int i = 0; i < 100; i++) {
                File to = new File(baseFile, String.format("config.old.%s.yml", i));
                if (!to.exists()) {
                    Files.move(configFile.toPath(), to.toPath());
                    saveDefaultConfig();
                    return;
                }
            }
            throw new ConfigurationException("unable to move config file, there are too many old configs");
        } catch (Exception e) {
            throw new ConfigurationException("error while regenerating old config file", e);
        }
    }
    
    public void saveDefaultConfig() throws IOException {
        baseFile.mkdirs();
        configFile.createNewFile();
        try (InputStream in = plugin.getResource("config.yml"); OutputStream os = new FileOutputStream(configFile)) {
            ByteStreams.copy(in, os);
        }
    }
    
}
