package me.taucu.bukkitcommandblocker.utils;

public class ConfigurationException extends RuntimeException {
    
    private static final long serialVersionUID = 1L;
    
    public ConfigurationException(String m) {
        super(m);
    }
    
    public ConfigurationException(String m, Throwable t) {
        super(m, t);
    }
    
}
