/**
 * Copyright (c) 2021, Tau <nullvoxel@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Redistributions in source and/or binary forms must be free of all charges
 *    and fees to the recipient of the redistribution unless It is clearly
 *    disclosed to the recipient of the redistribution prior to payment that it
 *    includes this software or portions of it and by providing a means to
 *    obtain this software free of charge.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package me.taucu.bukkitcommandblocker.utils.text;

import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.chat.ComponentSerializer;

import java.util.Map;
import java.util.function.Function;

/**
 * Text utility class used by many of my plugins
 * @author TauCubed
 */
public class TextUtils {
    
    public static final TextComponent jnline = new TextComponent(ComponentSerializer.parse("{text: \"\n\"}"));
    
    public static String replacePlaceholders(String toReplace, Map<String, String> replacements) {
        return replacePlaceholders(toReplace, found -> {
            String val = replacements.get(found);
            if (val != null) {
                return val;
            }
            return found;
        });
    }
    
    public static String replacePlaceholders(String toReplace, Map<String, String> replacements, Function<String, String> onMissing) {
        return replacePlaceholders(toReplace, found -> {
            String val = replacements.get(found);
            if (val != null) {
                return val;
            }
            return onMissing.apply(found);
        });
    }
    
    public static String replacePlaceholders(String toReplace, Function<String, String> onFind) {
        return replacePlaceholders(toReplace, onFind, '%');
    }
    
    // 600 ns typical request
    public static String replacePlaceholders(String toReplace, Function<String, String> onFind, char placeholderChar) {
        int length = toReplace.length();
        
        int currentEscapeCount = 0;
        byte lastEscapeOffsetter = 0;
        int currentPlaceholderStart = -1;
        
        StringBuilder sb = new StringBuilder(length);
        int startPosition = 0;
        
        for (int i = 0; i < length; i++) {
            char at = toReplace.charAt(i);
            if (at == placeholderChar) {
                if (currentEscapeCount % 2 == 0) {
                    if (currentPlaceholderStart == -1) {
                        currentPlaceholderStart = i;
                    } else {
                        if (currentPlaceholderStart > 0 && startPosition < toReplace.length()) {
                            sb.append(toReplace, startPosition + lastEscapeOffsetter, currentPlaceholderStart);
                            lastEscapeOffsetter = 0;
                        }
                        
                        sb.append(onFind.apply(toReplace.substring(currentPlaceholderStart, i + 1)));
                        
                        startPosition = i + 1;
                        
                        currentPlaceholderStart = -1;
                    }
                } else {
                    currentEscapeCount = 0;
                }
            } else if (at == '\\') {
                currentEscapeCount++;
                lastEscapeOffsetter = 1;
            } else {
                currentEscapeCount = 0;
            }
        }
        
        if (toReplace.length() > 0 && startPosition < toReplace.length()) {
            sb.append(toReplace, startPosition, toReplace.length());
        }
        
        return sb.toString();
    }
    
}
