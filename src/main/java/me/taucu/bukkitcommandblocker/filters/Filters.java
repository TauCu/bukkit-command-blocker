package me.taucu.bukkitcommandblocker.filters;

import me.taucu.bukkitcommandblocker.filters.types.Blacklist;
import me.taucu.bukkitcommandblocker.filters.types.RestrictedBlacklist;
import me.taucu.bukkitcommandblocker.filters.types.RestrictedWhitelist;
import me.taucu.bukkitcommandblocker.filters.types.Whitelist;
import me.taucu.bukkitcommandblocker.utils.ConfigurationException;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Filters {
    
    private List<AbstractFilter> filters = new CopyOnWriteArrayList<AbstractFilter>();

    private String defaultPatternPrefix = "(?i)^\\/";
    private String defaultPatternSuffix = "(\\s|$)";
    private String defaultType = "blacklist";
    private String defaultDenyMsg = ChatColor.RED
            + "I'm sorry, but you do not have permission to perform this command. Please contact the server administrators if you believe that this is in error.";
    private String rootFilterPerm = "tau.bcmdblock.filter";
    
    private Map<String, FilterAction> actionAllowMap = Collections.synchronizedMap(new HashMap<String, FilterAction>());
    private Map<String, FilterAction> actionDenyMap = Collections.synchronizedMap(new HashMap<String, FilterAction>());
    
    private long permissionCacheMillis = 100;
    
    public Filters() {
    }
    
    public AbstractFilter apply(CommandSender s, String c) {
        AbstractFilter softDeny = null;
        for (AbstractFilter filter : filters) {
            switch (filter.apply(s, c)) {
                case ALLOW:
                    return null;
                case DENY:
                    return filter;
                case SOFT_ALLOW:
                    softDeny = null;
                    break;
                case SOFT_DENY:
                    softDeny = filter;
                    break;
                default:
                    break;
            }
        }
        return softDeny;
    }
    
    public String getDefaultPatternPrefix() {
        return defaultPatternPrefix;
    }
    
    public void setDefaultPatternPrefix(String defaultRegexPrefix) {
        this.defaultPatternPrefix = defaultRegexPrefix;
    }
    
    public String getDefaultPatternSuffix() {
        return defaultPatternSuffix;
    }
    
    public void setDefaultPatternSuffix(String defaultPatternSuffix) {
        this.defaultPatternSuffix = defaultPatternSuffix;
    }
    
    public String getDefaultType() {
        return defaultType;
    }
    
    public void setDefaultType(String defaultType) {
        this.defaultType = defaultType;
    }
    
    public FilterAction getDefaultAllowActionFor(String type) {
        FilterAction val = actionAllowMap.get(type.toLowerCase());
        if (val == null) {
            throw new NoSuchElementException("No default allow action found for \"" + type + "\"");
        } else {
            return val;
        }
    }
    
    public FilterAction getDefaultDenyActionFor(String type) {
        FilterAction val = actionDenyMap.get(type.toLowerCase());
        if (val == null) {
            throw new NoSuchElementException("No default deny action found for \"" + type + "\"");
        } else {
            return val;
        }
    }
    
    public void setDefaultAllowActionFor(String type, String action) {
        type = type.toLowerCase();
        action = action.toUpperCase();
        
        actionAllowMap.put(type, actionByName(action));
    }
    
    public void setDefaultDenyActionFor(String type, String action) {
        type = type.toLowerCase();
        action = action.toUpperCase();
        
        actionDenyMap.put(type, actionByName(action));
    }
    
    public String defaultDenyMsg() {
        return defaultDenyMsg;
    }
    
    public void setDefaultDenyMsg(String m) {
        this.defaultDenyMsg = m;
    }
    
    public String getRootFilterPerm() {
        return rootFilterPerm;
    }
    
    public void setFilterPerm(String rootFilterPerm) {
        this.rootFilterPerm = rootFilterPerm;
    }
    
    public void setDefaultPermissionCacheMillis(long millis) {
        this.permissionCacheMillis = millis;
    }
    
    public List<AbstractFilter> getFilters() {
        return filters;
    }
    
    public void add(AbstractFilter f) {
        if (!filters.contains(f)) {
            filters.add(f);
        }
    }
    
    public void remove(AbstractFilter f) {
        filters.remove(f);
    }
    
    public void clear() {
        filters.clear();
    }
    
    public void loadDefaultActions(ConfigurationSection defaultActionsSection) {
        try {
            for (String k : defaultActionsSection.getKeys(false)) {
                String action;
                action = defaultActionsSection.getString(k + ".allow");
                if (action == null) {
                    throw new ConfigurationException("\"default actions\" is an invalid configuration section");
                } else {
                    setDefaultAllowActionFor(k, action);
                }
                action = defaultActionsSection.getString(k + ".deny");
                if (action == null) {
                    throw new ConfigurationException("\"default actions\" is an invalid configuration section");
                } else {
                    setDefaultDenyActionFor(k, action);
                }
            }
        } catch (Exception e) {
            throw new ConfigurationException("Error while loading a types default actions", e);
        }
    }
    
    public boolean load(Logger l, ConfigurationSection rt) {
        l.info("loading filters...");
        boolean errorless = true;
        List<String> keys = new ArrayList<String>(rt.getKeys(false));
        Comparator<String> com = new Comparator<String>() {
            @Override
            public int compare(String k1, String k2) {
                ConfigurationSection c1 = rt.getConfigurationSection(k1);
                ConfigurationSection c2 = rt.getConfigurationSection(k2);
                if (c1 != null && c2 != null) {
                    int p1, p2;
                    if (c1.contains("priority")) {
                        p1 = c1.getInt("priority");
                    } else {
                        p1 = Integer.MIN_VALUE;
                    }
                    if (c2.contains("priority")) {
                        p2 = c2.getInt("priority");
                    } else {
                        p2 = Integer.MIN_VALUE;
                    }
                    return p1 - p2;
                }
                return 0; // handle errors later
            }
        };
        Collections.sort(keys, com);
        
        List<AbstractFilter> filters = new ArrayList<AbstractFilter>();
        int patternCount = 0;
        for (String k : keys) {
            try {
                ConfigurationSection c = rt.getConfigurationSection(k);
                if (c == null) {
                    throw new ConfigurationException(k + " is not a valid configuration section");
                }
                List<String> patterns = c.getStringList("patterns");
                if (patterns == null) {
                    throw new ConfigurationException("\"" + k + "." + "patterns\" is not a valid string list");
                }
                final String type = c.getString("type", defaultType);
                AbstractFilter filter = filterOfType(type, k.toLowerCase());
                filter.setPattern(patterns.toArray(new String[patterns.size() - 1]));
                if (c.contains("pattern prefix")) {
                    filter.setPatternPrefix(c.getString("pattern prefix"));
                }
                if (c.contains("pattern suffix")) {
                    filter.setPatternPrefix(c.getString("pattern suffix"));
                }
                if (c.contains("message")) {
                    filter.setDenyMsg(ChatColor.translateAlternateColorCodes('&', c.getString("message")));
                }
                if (c.contains("allow action")) {
                    filter.setAllowAction(actionByName(c.getString("allow action")));
                } else {
                    filter.setAllowAction(getDefaultAllowActionFor(type));
                }
                if (c.contains("deny action")) {
                    filter.setDenyAction(actionByName(c.getString("deny action")));
                } else {
                    filter.setDenyAction(getDefaultDenyActionFor(type));
                }
                filters.add(filter);
                patternCount += patterns.size();
            } catch (Exception e) {
                l.log(Level.SEVERE, "could not load filter \"" + k + "\" with error: ", e);
                errorless = false;
            }
        }
        filters = new CopyOnWriteArrayList<AbstractFilter>(filters);
        Collections.reverse(filters);
        this.filters = filters;
        l.info("loaded " + filters.size() + " filters with " + patternCount + " pattern(s)");
        return errorless;
    }
    
    private AbstractFilter filterOfType(String type, String name) {
        if (type == null || type.isEmpty()) {
            type = defaultType;
        }
        switch (type.toLowerCase()) {
        case "whitelist":
            return new Whitelist(this, name);
        case "blacklist":
            return new Blacklist(this, name);
        case "restrictedwhitelist":
            return new RestrictedWhitelist(this, name);
        case "restrictedblacklist":
            return new RestrictedBlacklist(this, name);
        default:
            throw new NoSuchElementException("no filter found by type \"" + type + "\"");
        }
    }
    
    private FilterAction actionByName(String name) {
        if (name != null) {
            try {
                return FilterAction.valueOf(name.toUpperCase());
            } catch (IllegalArgumentException e) {
                throw new NoSuchElementException("no filter action found by the name of \"" + name + "\"");
            }
        } else {
            throw new NullPointerException("cannot get action by a null name \"" + name + "\"");
        }
    }
    
    public long getPermissionCacheMillis() {
        return permissionCacheMillis;
    }
    
}
