package me.taucu.bukkitcommandblocker.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import me.taucu.bukkitcommandblocker.CommandBlockerPlugin;
import me.taucu.bukkitcommandblocker.filters.AbstractFilter;
import me.taucu.bukkitcommandblocker.filters.Filters;

public class ChatListener implements Listener {
    
    private final Filters filters;
    
    public ChatListener(CommandBlockerPlugin pl) {
        this.filters = pl.getFilters();
    }
    
    @EventHandler
    public void onChat(PlayerCommandPreprocessEvent e) {
        AbstractFilter filter = filters.apply(e.getPlayer(), e.getMessage());
        if (filter != null) {
            e.setCancelled(true);
            e.getPlayer().sendMessage(filter.getDenyMsg());
        }
    }
    
}
