package me.taucu.bukkitcommandblocker.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.TabCompleteEvent;

import me.taucu.bukkitcommandblocker.CommandBlockerPlugin;
import me.taucu.bukkitcommandblocker.filters.Filters;

public class TabCompleteListener implements Listener {
    
    private final Filters filters;
    
    public TabCompleteListener(CommandBlockerPlugin pl) {
        this.filters = pl.getFilters();
    }
    
    @EventHandler
    public void onTab(TabCompleteEvent e) {
        if (filters.apply(e.getSender(), e.getBuffer()) != null) {
            e.setCancelled(true);
        }
    }
    
}
