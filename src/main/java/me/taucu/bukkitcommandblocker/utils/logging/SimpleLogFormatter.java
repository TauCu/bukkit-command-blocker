/**
 * Copyright (c) 2021, Tau <nullvoxel@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Redistributions in source and/or binary forms must be free of all charges
 *    and fees to the recipient of the redistribution unless It is clearly
 *    disclosed to the recipient of the redistribution prior to payment that it
 *    includes this software or portions of it and by providing a means to
 *    obtain this software free of charge.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package me.taucu.bukkitcommandblocker.utils.logging;

import net.md_5.bungee.api.ChatColor;

import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;

public class SimpleLogFormatter extends Formatter {

    public static final SimpleLogFormatter INSTANCE = new SimpleLogFormatter(false);
    public static final SimpleLogFormatter INSTANCE_NEWLINE = new SimpleLogFormatter(true);
    public static final SimpleLogFormatter CHATCOLOR_INSTANCE_NEWLINE = new SimpleChatColorLogFormatter();
    public static final SimpleLogFormatter CHATCOLOR_INSTANCE = new SimpleChatColorLogFormatter();

    protected final boolean newline;

    public SimpleLogFormatter(boolean newline) {
        this.newline = newline;
    }

    @Override
    public String format(LogRecord record) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(record.getLevel().getLocalizedName());
        sb.append("]: [");
        sb.append(record.getLoggerName());
        sb.append("] ");
        sb.append(record.getMessage());
        if (newline) {
            sb.append('\n');
        }
        return sb.toString();
    }

    public String resolveLevel(LogRecord record) {
        return record.getLevel().getLocalizedName();
    }

    private static class SimpleChatColorLogFormatter extends SimpleLogFormatter {

        private final ChatColor defaultColor, warnColor, severeColor;

        public SimpleChatColorLogFormatter() {
            this(ChatColor.GRAY, ChatColor.YELLOW, ChatColor.RED);
        }

        public SimpleChatColorLogFormatter(ChatColor defaultColor, ChatColor warnColor, ChatColor severeColor) {
            this(false, defaultColor, warnColor, severeColor);
        }

        public SimpleChatColorLogFormatter(boolean newline, ChatColor defaultColor, ChatColor warnColor, ChatColor severeColor) {
            super(newline);
            this.defaultColor = defaultColor;
            this.warnColor = warnColor;
            this.severeColor = severeColor;
        }

        @Override
        public String format(LogRecord record) {
            Level level = record.getLevel();
            ChatColor color;
            switch (level.intValue()) {
                case 900:
                    color = warnColor;
                    break;
                case 1000:
                    color = severeColor;
                    break;
                default:
                    color = defaultColor;
                    break;
            }
            StringBuilder sb = new StringBuilder();
            sb.append(color + "[");
            sb.append(record.getLevel().getLocalizedName());
            sb.append("]: [");
            sb.append(record.getLoggerName());
            sb.append("] ");
            sb.append(record.getMessage());
            if (newline) {
                sb.append('\n');
            }
            return sb.toString();
        }
    }

}
