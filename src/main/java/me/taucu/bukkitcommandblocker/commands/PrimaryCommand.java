package me.taucu.bukkitcommandblocker.commands;

import me.taucu.bukkitcommandblocker.CommandBlockerPlugin;
import me.taucu.bukkitcommandblocker.utils.logging.LogUtils;
import me.taucu.bukkitcommandblocker.utils.logging.LogUtils.LoggerAttachment;
import me.taucu.bukkitcommandblocker.utils.text.TextUtils;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.stream.Collectors;

public class PrimaryCommand extends Command {
    
    private final CommandBlockerPlugin pl;
    
    private final BaseComponent[] reloading = (new ComponentBuilder("reloading config..."))
            .color(ChatColor.GOLD).create();
    private final BaseComponent[] reloaded = (new ComponentBuilder("reloaded!")).color(ChatColor.GREEN)
            .create();
    private final BaseComponent[] usage = (new ComponentBuilder("usage:"))
            .color(ChatColor.AQUA)
            .append(TextUtils.jnline)
            .append("/commandblocker reload")
            .append(TextUtils.jnline)
            .append("  - reloads this plugins config")
            .color(ChatColor.GRAY)
            .append(TextUtils.jnline)
            .append("/commandblocker syncCommands")
            .color(ChatColor.AQUA)
            .append(TextUtils.jnline)
            .append("  - fully recalculates all commands, filters them and sends them to all connected players")
            .color(ChatColor.GRAY)
            .append(TextUtils.jnline)
            .append("    (Warning: this can cause a lag spike for all players, especially Bedrock players)")
            .color(ChatColor.GRAY)
            .append(TextUtils.jnline)
            .append("/commandblocker sendCommands <player>")
            .color(ChatColor.AQUA)
            .append(TextUtils.jnline)
            .append("  - filters and sends an updated list of commands to the specified player")
            .color(ChatColor.GRAY)
            .append(TextUtils.jnline)
            .append("    (Warning: this can cause a lag spike for the player, especially Bedrock players)")
            .color(ChatColor.GRAY)
            .create();

    private final BaseComponent[] syncingCommands = (new ComponentBuilder("syncing commands..."))
            .color(ChatColor.GOLD).create();

    private final BaseComponent[] sendingCommands = (new ComponentBuilder("sending commands..."))
            .color(ChatColor.GOLD).create();
    
    public PrimaryCommand(CommandBlockerPlugin pl) {
        super("commandblocker", "used to manipulate command blocker", "/commandblocker <reload|synccommands|sendcommands>",
                Arrays.asList("cmdblock", "cmdblocker", "cmdblock", "cmdblocker", "cb"));
        this.setPermission("tau.cmdblock.command");
        this.pl = pl;
    }
    
    @SuppressWarnings("deprecation")
    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (args.length > 0) {
            switch (args[0].toLowerCase()) {
                case "reload":
                    if (args.length == 1) {
                        sender.sendMessage(reloading);
                        if (sender instanceof ConsoleCommandSender) {
                            pl.getConfigLoader().reloadConfig();
                        } else {
                            try (LoggerAttachment attach = LogUtils.attachLogger(pl.getLogger(), sender)) {
                                pl.getConfigLoader().reloadConfig();
                            }
                        }
                        sender.sendMessage(reloaded);
                    } else {
                        sender.sendMessage("Usage: /" + commandLabel + " reload");
                    }
                    return true;
                case "synccommands":
                    if (args.length == 1) {
                        sender.sendMessage(syncingCommands);
                        pl.getLogger().info("full command sync invoked by " + sender.getName());
                        if (sender instanceof ConsoleCommandSender) {
                            syncCommands();
                        } else {
                            try (LoggerAttachment attach = LogUtils.attachLogger(pl.getLogger(), sender)) {
                                syncCommands();
                            }
                        }
                    } else {
                        sender.sendMessage("Usage: /" + commandLabel + " synccommands");
                    }
                    return true;
                case "sendcommands":
                    if (args.length == 2) {
                        Player target = Bukkit.getPlayer(args[1]);
                        if (target == null) {
                            sender.sendMessage("player \"" + args[1] + "\" not found");
                        } else {
                            sender.sendMessage(sendingCommands);
                            sendCommands(target);
                        }
                    } else {
                        sender.sendMessage("Usage: /" + commandLabel + " sendcommands <player>");
                    }
                    return true;
                default: break;
            }
        }
        sender.sendMessage(usage);
        return true;
    }
    
    @Override
    public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        if (args.length > 0) {
            if (args.length > 1) {
                switch (args[0].toLowerCase()) {
                    case "sendcommands":
                        if (args.length == 2) {
                            return StringUtil.copyPartialMatches(args[1], Bukkit.getOnlinePlayers().stream().map(Player::getName).collect(Collectors.toList()), new ArrayList<String>());
                        }
                    default: break;
                }
                return Arrays.asList("");
            } else {
                return StringUtil.copyPartialMatches(args[0], Arrays.asList("reload", "syncCommands", "sendCommands"), new ArrayList<String>());
            }
        } else {
            return Arrays.asList("reload", "syncCommands", "sendCommands");
        }
    }

    public void syncCommands() {
        try {
            Method syncCommands = Bukkit.getServer().getClass().getDeclaredMethod("syncCommands", null);
            syncCommands.setAccessible(true);
            syncCommands.invoke(Bukkit.getServer(), null);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            pl.getLogger().log(Level.SEVERE, "Could not invoke sync commands method. Make sure this server is a supported version before making an issue.", e);
        }
    }

    public void sendCommands(Player target) {
        target.updateCommands();
    }
    
}
