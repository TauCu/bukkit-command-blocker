package me.taucu.bukkitcommandblocker.listeners;

import java.util.Iterator;

import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandSendEvent;

import me.taucu.bukkitcommandblocker.CommandBlockerPlugin;
import me.taucu.bukkitcommandblocker.filters.Filters;

public class DeclareCommandsListener implements Listener {
    
    private final Filters filters;
    
    public DeclareCommandsListener(CommandBlockerPlugin pl) {
        this.filters = pl.getFilters();
    }
    
    @EventHandler
    public void onCommandSend(PlayerCommandSendEvent e) {
        Iterator<String> it = e.getCommands().iterator();
        CommandSender sender = e.getPlayer();
        while (it.hasNext()) {
            if (filters.apply(sender, "/".concat(it.next())) != null) {
                it.remove();
            }
        }
    }
    
}
