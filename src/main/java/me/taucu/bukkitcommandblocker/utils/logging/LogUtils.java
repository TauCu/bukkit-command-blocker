/**
 * Copyright (c) 2021, Tau <nullvoxel@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Redistributions in source and/or binary forms must be free of all charges
 *    and fees to the recipient of the redistribution unless It is clearly
 *    disclosed to the recipient of the redistribution prior to payment that it
 *    includes this software or portions of it and by providing a means to
 *    obtain this software free of charge.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package me.taucu.bukkitcommandblocker.utils.logging;

import org.bukkit.command.CommandSender;

import java.util.function.Consumer;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * Logger utility class making it easy to attach closable handlers
 * @author TauCubed
 */
public class LogUtils {

    /**
     * Creates an {@link AutoCloseable} wrapper for a handler and attaches it to this {@link Logger}
     * <p>
     * The handler will use the {@link SimpleLogFormatter#CHATCOLOR_INSTANCE} as its formatter
     * @param log the {@link Logger} to attach to
     * @param sender the {@link CommandSender} to forward logs to via {@link CommandSender#sendMessage(String)}
     * @return the {@link LoggerAttachment}
     */
    public static LoggerAttachment attachLogger(Logger log, CommandSender sender) {
        return attachToLogger(log, sender::sendMessage);
    }

    /**
     * Creates an {@link AutoCloseable} wrapper for a handler and attaches it to this {@link Logger}
     * @param log the {@link Logger} to attach to
     * @param formatter the {@link Formatter} to use
     * @param sender the {@link CommandSender} to forward logs to via {@link CommandSender#sendMessage(String)}
     * @return the {@link LoggerAttachment}
     */
    public static LoggerAttachment attachLogger(Logger log, Formatter formatter, CommandSender sender) {
        return attachToLogger(log, formatter, sender::sendMessage);
    }

    /**
     * Creates an {@link AutoCloseable} wrapper for a handler and attaches it to this {@link Logger}
     * <p>
     * The handler will use the {@link SimpleLogFormatter#CHATCOLOR_INSTANCE} as its formatter
     * @param log the {@link Logger} to attach to
     * @param consumer the {@link Consumer} to forward logs to
     * @return the {@link LoggerAttachment}
     */
    public static LoggerAttachment attachToLogger(Logger log, Consumer<String> consumer) {
        return attachToLogger(log, SimpleLogFormatter.CHATCOLOR_INSTANCE, consumer);
    }

    /**
     * Creates an {@link AutoCloseable} wrapper for a handler and attaches it to this {@link Logger}
     * @param log the {@link Logger} to attach to
     * @param formatter the {@link Formatter} to use
     * @param consumer the {@link Consumer} to forward logs to
     * @return the {@link LoggerAttachment}
     */
    public static LoggerAttachment attachToLogger(Logger log, Formatter formatter, Consumer<String> consumer) {
        LoggerAttachment attachment = new LoggerAttachment(log, formatter, consumer);
        return attachment;
    }

    public static class LoggerAttachment implements AutoCloseable {

        private final Logger log;
        private volatile boolean open = true;
        private Consumer<String> logConsumer;

        public LoggerAttachment(Logger log, Consumer<String> logConsumer) {
            this(log, null, logConsumer);
        }

        public LoggerAttachment(Logger log, Formatter formatter, Consumer<String> logConsumer) {
            this.log = log;
            this.logConsumer = logConsumer;
            internalHandler.setFormatter(formatter);
            log.addHandler(internalHandler);
        }

        private final Handler internalHandler = new Handler() {

            @Override
            public void publish(LogRecord record) {
                if (open) {
                    Formatter formatter = getFormatter();
                    if (null == formatter) {
                        logConsumer.accept(record.getMessage());
                    } else {
                        logConsumer.accept(getFormatter().format(record));
                    }
                } else {
                    throw new RuntimeException("handler is closed");
                }
            }

            @Override
            public void flush() {
                if (!open) {
                    throw new RuntimeException("handler is closed");
                }
            }

            @Override
            public void close() throws SecurityException {
                try {
                    log.removeHandler(this);
                    flush();
                } finally {
                    open = false;
                }
            }
        };

        /**
         * Detaches and closes the wrapped {@link Handler}
         */
        @Override
        public void close() {
            internalHandler.close();
        }

        /**
         * checks if this attachment is closed
         * @return true of the attachment is closed, false otherwise
         */
        public boolean isClosed() {
            return !open;
        }

    }

    /**
     * Gets a logger by this name (ideally creating one)
     * disables parent handlers and attaches the provided handler setting its formatter
     * @param name The logger name
     * @param formatter the formatter to be used
     * @param handler the handler to use for this logger
     * @return the logger
     */
    public static Logger newLogger(String name, Formatter formatter, Handler handler) {
        Logger log = Logger.getLogger(name);
        log.setUseParentHandlers(false);
        handler.setFormatter(formatter);
        log.addHandler(handler);
        return log;
    }
}
