package me.taucu.bukkitcommandblocker;

import me.taucu.bukkitcommandblocker.commands.PrimaryCommand;
import me.taucu.bukkitcommandblocker.filters.Filters;
import me.taucu.bukkitcommandblocker.listeners.ChatListener;
import me.taucu.bukkitcommandblocker.listeners.DeclareCommandsListener;
import me.taucu.bukkitcommandblocker.listeners.TabCompleteListener;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.Iterator;

public class CommandBlockerPlugin extends JavaPlugin {
    
    private final Filters filters = new Filters();
    
    private final ConfigLoader configLoader = new ConfigLoader(this, new File("./plugins/BukkitCommandBlocker/"));

    private PrimaryCommand primaryCommand = null;
    
    @Override
    public void onEnable() {
        registerCommands();
        configLoader.reloadConfig();
        registerEvents(new ChatListener(this));
        registerEvents(new TabCompleteListener(this));
        registerEvents(new DeclareCommandsListener(this));
    }
    
    @Override
    public void onDisable() {
        // support plugin loaders.
        // many of them can't seem to figure out how the command map works (thus creating a memory leak) so let's help them...

        // this doesn't work. why?
        primaryCommand.unregister(Bukkit.getCommandMap());
        // this, however, does work.
        Iterator<Command> it = Bukkit.getCommandMap().getKnownCommands().values().iterator();
        while (it.hasNext()) {
            if (it.next() == primaryCommand) {
                it.remove();
            }
        }
        primaryCommand.syncCommands();
    }
    
    public ConfigLoader getConfigLoader() {
        return configLoader;
    }
    
    public Filters getFilters() {
        return filters;
    }
    
    public void registerCommands() {
        primaryCommand = new PrimaryCommand(this);
        Bukkit.getCommandMap().register("bukkitcommandblocker", primaryCommand);
    }
    
    public <T extends Listener> T registerEvents(T l) {
        Bukkit.getPluginManager().registerEvents(l, this);
        return l;
    }

    public boolean isShuttingDown() {
        try {
            // doesn't exist in old versions
            return Bukkit.isStopping();
        } catch (Exception ignored) {}
        return true;
    }
    
}
