package me.taucu.bukkitcommandblocker.utils;

public class EnumUtils {
    
    public static <T extends Enum<T>> T valueOf(Class<T> t, String s) {
        try {
            return Enum.valueOf(t, s);
        } catch (IllegalArgumentException a) {
            return null;
        }
    }
    
}
